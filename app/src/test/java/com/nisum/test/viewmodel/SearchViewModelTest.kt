package com.nisum.test.viewmodel

import android.os.Build
import com.nisum.test.api.status.Result
import com.nisum.test.repository.SearchRepository
import com.nisum.test.utils.getOrAwaitValue
import com.nisum.test.utils.mock.SearchResponseMock.getResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@ExperimentalCoroutinesApi
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class SearchViewModelTest {

    private val searchRepositoryMock = Mockito.mock(SearchRepository::class.java)

    private lateinit var viewModel: SearchViewModel

    @Before
    fun setUp() {
        viewModel = SearchViewModel().apply {
            searchRepository = searchRepositoryMock
        }
    }

    @Test
    fun search_shouldCallRepository_andSearchResultSuccess() = runBlockingTest {
        val searchResponseMock = getResponse()
        Mockito.`when`(searchRepositoryMock.search("term", "mediaType", "limit")).thenReturn(Result.success(searchResponseMock))

        val expected = SearchViewModel.SearchUiState.Success(searchResponseMock)

        viewModel.search("term", "mediaType", "limit")

        assertEquals(expected, viewModel.uiState.getOrAwaitValue())
    }

    @Test
    fun search_shouldCallRepository_andSearchResultError() = runBlockingTest {
        Mockito.`when`(searchRepositoryMock.search("term", "mediaType", "limit")).thenReturn(Result.serverError())

        val expected = SearchViewModel.SearchUiState.ServerError

        viewModel.search("term", "mediaType", "limit")

        assertEquals(viewModel.uiState.getOrAwaitValue(), expected)
    }
}