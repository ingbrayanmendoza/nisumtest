package com.nisum.test.repository

import com.google.gson.Gson
import com.nisum.test.api.status.Status
import com.nisum.test.utils.mock.SearchResponseMock.getResponse
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.ResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import retrofit2.Response

class SearchRepositoryTest {

    private companion object {
        val mediaType = "application/json; charset=utf-8".toMediaType()
        val body = Gson().toJson(getResponse()).toResponseBody(mediaType)
        var successResponse: Response<ResponseBody> = Response.success(body)
        var errorResponse: Response<ResponseBody> = Response.error<ResponseBody>(400, body)
    }

    private lateinit var repository: SearchRepository
    private var searchServiceMock = Mockito.mock(SearchService::class.java)

    @Before
    fun setUp() {
        repository = Mockito.spy(SearchRepository()).apply {
            searchService = searchServiceMock
        }
    }

    @Test
    fun search_whenIsSuccessful_shouldReturnResponseSuccess() {
        runBlocking {
            Mockito.`when`(searchServiceMock.searchMedias("term", "mediaType", "limit")).thenReturn(successResponse)

            val result = repository.search("term", "mediaType", "limit")
            assertEquals(result.status, Status.SUCCESS)
        }
    }

    @Test
    fun search_whenIsFail_shouldReturnResponseError() {
        runBlocking {
            Mockito.`when`(searchServiceMock.searchMedias("term", "mediaType", "limit")).thenReturn(errorResponse)

            val result = repository.search("term", "mediaType", "limit")
            assertEquals(result.status, Status.SERVER_ERROR)
        }
    }
}