package com.nisum.test.utils.mock

import com.nisum.test.model.Media
import com.nisum.test.model.SearchResponse

object SearchResponseMock {

    fun getResponse() = SearchResponse (20, getMedias())

    fun getMedias(): ArrayList<Media> {
        return arrayListOf(
            Media(
                2,
                1,
                "artistName",
                "collectionName",
                "trackName",
                "artistViewUrl",
                "collectionViewUrl",
                "trackViewUrl",
                "previewUrl",
                "artworkUrl100",
                10f,
                1f,
                15f,
                1.5f,
                "country",
                "currency",
                "longDescription"
            )
        )
    }
}