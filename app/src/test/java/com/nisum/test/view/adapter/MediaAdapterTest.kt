package com.nisum.test.view.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.nisum.test.databinding.MediaAdapterBinding
import com.nisum.test.model.Media
import com.nisum.test.utils.TestApplication
import com.nisum.test.utils.mock.SearchResponseMock.getMedias
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P], application = TestApplication::class)
class MediaAdapterTest {

    private lateinit var adapter: MediaAdapter
    private lateinit var medias: List<Media>
    private val onClickListener = object :
        MediaAdapter.OnClickItemListener {
        override fun onClick(media: Media) {
            // Do nothing
        }
    }

    @Before
    fun setUp() {
        medias = getMedias()
        adapter = MediaAdapter(medias, onClickListener)
    }

    @Test
    fun onCreateViewHolder_shouldReturnDetailViewHolder() {
        val view: ViewGroup = LinearLayout(getApplicationContext())
        Assert.assertNotNull(adapter.onCreateViewHolder(view, 0))
    }

    @Test
    fun onBindViewHolder_shouldBindViewHolder() {
        val binding = MediaAdapterBinding.inflate(LayoutInflater.from(getApplicationContext()))
        val viewHolder = Mockito.spy(adapter.MediaViewHolder(binding))

        adapter.onBindViewHolder(viewHolder, 0)

        Mockito.verify(viewHolder).bind(medias[0])
    }

    @Test
    fun getItemCount_shouldReturnDetailsSize() {
        Assert.assertEquals(medias.size, adapter.itemCount)
    }
}