package com.nisum.test.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nisum.test.api.status.Status.SERVER_ERROR
import com.nisum.test.api.status.Status.SUCCESS
import com.nisum.test.model.SearchResponse
import com.nisum.test.repository.SearchRepository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SearchViewModel: ViewModel() {

    var searchRepository = SearchRepository()

    private val _uiState = MutableLiveData<SearchUiState>()
    val uiState: LiveData<SearchUiState>
        get() = _uiState

    private fun showLoading() {
        _uiState.value = SearchUiState.Loading
    }

    private val coroutineExceptionHandler = CoroutineExceptionHandler { _, _ ->
        _uiState.value = SearchUiState.ConnectionError
    }

    fun search(term: String, mediaType: String, limit: String) {
        viewModelScope.launch(Dispatchers.Main + coroutineExceptionHandler) {
            showLoading()
            val response = searchRepository.search(term, mediaType, limit)
            when (response.status) {
                SUCCESS -> {
                    response.data?.let { data -> _uiState.value = SearchUiState.Success(data) }
                }
                SERVER_ERROR -> { _uiState.value = SearchUiState.ServerError }
            }
        }
    }

    sealed class SearchUiState {
        object Loading : SearchUiState()
        object ServerError : SearchUiState()
        object ConnectionError : SearchUiState()
        data class Success(val data: SearchResponse) : SearchUiState()
    }
}
