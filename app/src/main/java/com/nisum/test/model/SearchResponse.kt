package com.nisum.test.model

data class SearchResponse (
    val resultCount: Int,
    val results: ArrayList<Media>
)