package com.nisum.test.model

import java.io.Serializable

data class Media(
    val artistId: Int,
    val collectionId: Int,
    val artistName: String,
    val collectionName: String,
    val trackName: String,
    val artistViewUrl: String,
    val collectionViewUrl: String,
    val trackViewUrl: String,
    val previewUrl: String,
    val artworkUrl100: String,
    val collectionPrice: Float,
    val trackPrice: Float,
    val collectionHdPrice: Float,
    val trackHdPrice: Float,
    val country: String,
    val currency: String,
    val longDescription: String
) : Serializable