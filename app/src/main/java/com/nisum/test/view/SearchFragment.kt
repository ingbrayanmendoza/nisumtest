package com.nisum.test.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.View.GONE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.nisum.test.R
import com.nisum.test.databinding.SearchFragmentBinding
import com.nisum.test.extension.hideKeyboard
import com.nisum.test.model.Media
import com.nisum.test.model.SearchResponse
import com.nisum.test.view.DetailsFragment.Companion.MEDIA_KEY
import com.nisum.test.view.FeedbackFragment.Companion.TITLE_KEY
import com.nisum.test.view.adapter.MediaAdapter
import com.nisum.test.viewmodel.SearchViewModel
import com.nisum.test.viewmodel.SearchViewModel.SearchUiState

class SearchFragment: Fragment(), MediaAdapter.OnClickItemListener {

    private lateinit var binding: SearchFragmentBinding
    private val viewModel: SearchViewModel by activityViewModels()

    private val uiStateObserver: Observer<SearchUiState> =
        Observer { uiStateResponse ->
            when (uiStateResponse) {
                is SearchUiState.Loading -> showLoading()
                is SearchUiState.ServerError -> showServerErrorScreen()
                is SearchUiState.ConnectionError -> showConnectionErrorScreen()
                is SearchUiState.Success -> bindScreen(uiStateResponse.data)
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SearchFragmentBinding.inflate(inflater, container, false)
        viewModel.uiState.observe(viewLifecycleOwner, uiStateObserver)
        binding.apply {
            buttonSearch.setOnClickListener {
                requireActivity().hideKeyboard()
                viewModel.search(search.text.toString(),"music", "20")
            }
        }
        return binding.root
    }

    private fun bindScreen(data: SearchResponse) {
        hideLoading()
        with(binding) {
            medias.visibility = View.VISIBLE
            medias.adapter = MediaAdapter(data.results, this@SearchFragment)
            search.text.clear()
        }
    }

    private fun hideLoading() {
        binding.loading.visibility = GONE
    }

    private fun showConnectionErrorScreen() {
        val bundle = Bundle().apply {
            putString(TITLE_KEY, resources.getString(R.string.title_connection_error))
        }
        findNavController().navigate(R.id.action_searchFragment_to_feedbackFragment, bundle)
    }

    private fun showServerErrorScreen() {
        val bundle = Bundle().apply {
            putString(TITLE_KEY, resources.getString(R.string.title_server_error))
        }
        findNavController().navigate(R.id.action_searchFragment_to_feedbackFragment, bundle)
    }

    private fun showLoading() {
        binding.loading.visibility = VISIBLE
    }

    override fun onClick(media: Media) {
        val bundle = Bundle().apply { putSerializable(MEDIA_KEY, media) }
        findNavController().navigate(R.id.action_searchFragment_to_detailsFragment, bundle)
    }
}