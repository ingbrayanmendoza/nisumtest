package com.nisum.test.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.nisum.test.databinding.FeedbackFragmentBinding

class FeedbackFragment: Fragment() {

    companion object {
        const val TITLE_KEY = "title_key"
    }

    private lateinit var binding: FeedbackFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FeedbackFragmentBinding.inflate(inflater, container, false)
        arguments?.let {
            bindScreen(
                it.getString(TITLE_KEY, "")
            )
        }
        return binding.root
    }

    private fun bindScreen(title: String) {
        with (binding) {
            this.title.text = title
            buttonRetry.setOnClickListener { findNavController().navigateUp() }
        }
    }
}