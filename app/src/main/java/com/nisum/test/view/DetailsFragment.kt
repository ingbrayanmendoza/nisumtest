package com.nisum.test.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.nisum.test.R
import com.nisum.test.databinding.DetailsFragmentBinding
import com.nisum.test.model.Media
import com.nisum.test.view.PlayerFragment.Companion.URL_KEY

class DetailsFragment: Fragment() {

    companion object {
        const val MEDIA_KEY = "media_key"
    }

    private lateinit var binding: DetailsFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DetailsFragmentBinding.inflate(inflater, container, false)
        bindScreen(arguments?.getSerializable(MEDIA_KEY) as Media)
        return binding.root
    }

    private fun bindScreen(media: Media) {
        with(binding) {
            seePreview.setOnClickListener {
                val bundle = Bundle().apply {
                    putString(URL_KEY, media.previewUrl)
                }
                findNavController().navigate(R.id.action_detailsFragment_to_playerFragment, bundle)
            }
            preview.setImageURI(media.artworkUrl100)
            track.text = media.trackName
            if (media.longDescription.isNullOrBlank()) description.visibility = GONE else description.text = media.longDescription
            collection.text = media.collectionName
            artist.text = media.artistName
        }
    }
}