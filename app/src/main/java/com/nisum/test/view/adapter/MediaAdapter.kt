package com.nisum.test.view.adapter

import android.view.LayoutInflater.from
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.nisum.test.databinding.MediaAdapterBinding
import com.nisum.test.model.Media
import com.nisum.test.view.adapter.MediaAdapter.MediaViewHolder

class MediaAdapter(
    private val medias: List<Media>,
    private val onClickItemListener: OnClickItemListener
): Adapter<MediaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaViewHolder {
        return MediaViewHolder(MediaAdapterBinding.inflate(from(parent.context)))
    }

    override fun getItemCount() = medias.size

    override fun onBindViewHolder(holder: MediaViewHolder, position: Int) {
        holder.bind(medias[position])
    }

    inner class MediaViewHolder(private val binding: MediaAdapterBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(media: Media) {
            with(binding) {
                container.setOnClickListener { onClickItemListener.onClick(media) }
                preview.setImageURI(media.artworkUrl100)
                track.text = media.trackName
                artist.text = media.artistName
            }
        }
    }

    interface OnClickItemListener {
        fun onClick(media: Media)
    }
}
