package com.nisum.test.view

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.nisum.test.databinding.PlayerFragmentBinding

class PlayerFragment: Fragment() {

    companion object {
        const val URL_KEY = "url_key"
    }

    private lateinit var binding: PlayerFragmentBinding
    private lateinit var exoPlayer: SimpleExoPlayer


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PlayerFragmentBinding.inflate(inflater, container, false)
        arguments?.let { initializePlayer(it.getString(URL_KEY, "")) }
        return binding.root
    }

    private fun buildMediaSource(url: String): MediaSource? {
        val dataSourceFactory = DefaultDataSourceFactory(requireContext(), "sample")
        return ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse(url))
    }

    private fun initializePlayer(url: String) {
        exoPlayer = SimpleExoPlayer.Builder(requireContext()).build()
        binding.playerControlView.player = exoPlayer
        binding.exoPlayerView.player = exoPlayer
        buildMediaSource(url)?.let {
            exoPlayer.prepare(it)
            exoPlayer.play()
        }

    }

    override fun onStop() {
        super.onStop()
        exoPlayer.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        exoPlayer.release()
    }
}
