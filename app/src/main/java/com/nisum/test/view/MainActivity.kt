package com.nisum.test.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.nisum.test.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }
}