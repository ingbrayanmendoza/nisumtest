package com.nisum.test.api.status

enum class Status {
    SUCCESS,
    SERVER_ERROR
}
