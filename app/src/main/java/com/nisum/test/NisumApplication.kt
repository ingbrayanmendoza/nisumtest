package com.nisum.test

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

class NisumApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        initFresco()
    }

    private fun initFresco() {
        Fresco.initialize(applicationContext)
    }
}