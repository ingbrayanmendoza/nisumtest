package com.nisum.test.repository

import com.google.gson.Gson
import com.nisum.test.api.RetrofitBuilder
import com.nisum.test.api.status.Result
import com.nisum.test.model.SearchResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SearchRepository {

    var searchService = RetrofitBuilder.getClient().create(SearchService::class.java)

    suspend fun search(term: String, mediaType: String, limit: String): Result<SearchResponse> =
        withContext(Dispatchers.IO) {
            val response = searchService.searchMedias(term, mediaType, limit)
            return@withContext if (response.isSuccessful) {
                Result.success(
                    Gson().fromJson(
                        response.body()?.charStream(),
                        SearchResponse::class.java
                    )
                )
            } else
                Result.serverError()
        }
}
