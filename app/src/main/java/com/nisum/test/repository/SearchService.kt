package com.nisum.test.repository

import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchService {
    @GET("search")
    suspend fun searchMedias(
        @Query("term") term: String,
        @Query("mediaType") mediaType: String,
        @Query("limit") limit: String
    ): Response<ResponseBody>
}